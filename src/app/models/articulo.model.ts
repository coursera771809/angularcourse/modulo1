export class Articulo{
    titulo:string;
    subtitulo:string;
    contenido:string;
    palabras_clave: string;
    fecha:string;
    date:Date;

    constructor(titulo:string, subtitulo:string, contenido:string, palabras_clave:string){
        this.titulo = titulo;
        this.subtitulo = subtitulo;
        this.contenido = contenido;
        this.palabras_clave= palabras_clave;
        this.date = new Date();
        this.fecha =
        this.format(this.date.getFullYear(), 4) +
        '/' +
        this.format(this.date.getMonth() + 1, 2) +
        '/' +
        this.format(this.date.getDate(), 2) +
        ' ' +
        this.format(this.date.getHours(), 2) +
        ':' +
        this.format(this.date.getMinutes(), 2) +
        ':' +
        this.format(this.date.getSeconds(), 2);
    }

    format = (input: number, padLength: number): string => {
        return input.toString().padStart(padLength, '0');
    };

}