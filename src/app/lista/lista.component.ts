import { Component, HostBinding, OnInit } from '@angular/core';
import { Articulo } from '../models/articulo.model';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  articulos:Articulo[];

  constructor() {
    this.articulos = [];
  }

  ngOnInit(): void {
  }

  guardar(titulo:string,subtitulo:string,contenido:string,palabras_clave:string){
    this.articulos.push(new Articulo(titulo,subtitulo,contenido,palabras_clave));
    return false;
  }

}
