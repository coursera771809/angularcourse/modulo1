import { Component, HostBinding, Input, OnInit } from '@angular/core';
import { Articulo } from '../models/articulo.model';

@Component({
  selector: 'app-componente',
  templateUrl: './componente.component.html',
  styleUrls: ['./componente.component.css']
})
export class ComponenteComponent implements OnInit {
  @Input() articulo:Articulo;
  @HostBinding('attr.class') cssClass = 'row-flex col-4';

  constructor(){}

  ngOnInit(): void {
  }

}
